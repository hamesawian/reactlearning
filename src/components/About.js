import React from 'react'
import SyntaxHighlighter from 'react-syntax-highlighter';
import { lightfair } from 'react-syntax-highlighter/dist/esm/styles/hljs';


export default function About() {


  const codeString1 = `class Car extends React.Component {
    render() {
      return <h2>Hi, I am a Car!</h2>;
    }
  }`

  const codeString2 =`function Car() {
    return <h2>Hi, I am a Car!</h2>;
  }`

  const codeString3  = `const root = ReactDOM.createRoot(document.getElementById('root'));
  root.render(<Car />);`


  const codeString4 = `function Car(props) {
    return <h2>I am a {props.color} Car!</h2>;
  }
  
  const root = ReactDOM.createRoot(document.getElementById('root'));
  root.render(<Car color="red"/>);`


  const codeString5 = `function Car() {
    return <h2>I am a Car!</h2>;
  }
  
  function Garage() {
    return (
      <>
        <h1>Who lives in my Garage?</h1>
        <Car />
      </>
    );
  }
  
  const root = ReactDOM.createRoot(document.getElementById('root'));
  root.render(<Garage />);`

  const codeString6 =`function Car() {
    return <h2>Hi, I am a Car!</h2>;
  }
  
  export default Car;`

  const codeString7 =`import React from 'react';
  import ReactDOM from 'react-dom/client';
  import Car from './Car.js';
  
  const root = ReactDOM.createRoot(document.getElementById('root'));
  root.render(<Car />);`

  return (
  
    <div className="Maincontent" id="main">
      
     <h1>React Components</h1>
      <hr/>

<p className="intro">Components are like functions that return HTML elements.</p>

<hr/>
<h2>React Components</h2>
<p>Components are independent and reusable bits of code.
They serve the same purpose as JavaScript functions,
but work in isolation and return HTML.</p>
<p>Components come in two types, className components and Function components, in 
this tutorial we will concentrate on Function components.</p>


<hr/>
<h2>Create Your First Component</h2>

<p>When creating a React component, the component's name <em>MUST</em> start with an 
upper case letter.</p>

<hr/>

<h3>className Component</h3>
<p>A className component must include the <code className="w3-codespan">extends React.Component</code> statement. 
This statement creates an inheritance to React.Component, and gives your component access to React.Component's functions.</p>

<p>The component also requires a <code className="w3-codespan">render()</code> method, 
this method returns HTML.</p>

<div className="static-text">
<h3>Example</h3>
<p>Create a className component called <code className="w3-codespan">Car</code></p>

<SyntaxHighlighter language="javascript" style={lightfair} showLineNumbers={true}>
{codeString1}
</SyntaxHighlighter>

<br/>
</div>

<hr/>

<h3>Function Component</h3>
<p>Here is the same example as above, but created using a Function component instead.</p>

<p>A Function component also returns HTML, and behaves much the same way as a className component,
but Function components can be written using much less code,
are easier to understand, and will be preferred in this tutorial.</p>

<div className="static-text">
<h3>Example</h3>
<p>Create a Function component called <code className="w3-codespan">Car</code></p>

<SyntaxHighlighter language="javascript" style={lightfair} showLineNumbers={true}>

    {codeString2}

</SyntaxHighlighter>



<br/>
</div>


<hr/>
<h2>Rendering a Component</h2>
<p>Now your React application has a component called Car, which returns an 
<code className="w3-codespan">&lt;h2&gt;</code> element.</p>
<p>To use this component in your application, use similar syntax as normal HTML:
<code className="w3-codespan">&lt;Car /&gt;</code></p>

<div className="static-text">
<h3>Example</h3>
<p>Display the <code className="w3-codespan">Car</code> component in the "root" element:</p>
<SyntaxHighlighter className="runexample" language="javascript" style={lightfair} showLineNumbers={true}>
  {codeString3}
</SyntaxHighlighter>
<p>
<a target="_blank" className="w3-btn" href="https://www.w3schools.com/REACT/showreact.asp?filename=demo2_react_component_function">Run 
Example »</a>
</p>
</div>

<hr/>

<h2>Props</h2>

<p>Components can be passed as <code className="w3-codespan">props</code>, which stands for properties.</p>

<p>Props are like function arguments, and you send them into the component as attributes.</p>
<p>You will learn more about <code className="w3-codespan">props</code> in the next chapter.</p>
<div className="static-text">
  <h3>Example</h3>
<p>Use an attribute to pass a color to the Car component, and use it in the 
render() function:</p>

<SyntaxHighlighter className="runexample" language="javascript" style={lightfair} showLineNumbers={true}>
  {codeString4}
</SyntaxHighlighter>

<p>
<a target="_blank" className="w3-btn" href="https://www.w3schools.com/REACT/showreact.asp?filename=demo2_react_component_props">Run 
Example »</a>
</p>
</div>

<hr/>

<h2>Components in Components</h2>

<p>We can refer to components inside other components:</p>

<div className="static-text">
<h3>Example</h3>
<p>Use the Car component inside the Garage component:</p>

<SyntaxHighlighter className="runexample" language="javascript" style={lightfair} showLineNumbers={true}>
  {codeString5}
</SyntaxHighlighter>

<p>
<a target="_blank" className="w3-btn" href="https://www.w3schools.com/REACT/showreact.asp?filename=demo2_react_component_many">Run 
Example »</a>
</p>
</div>

<hr/>

<h2>Components in Files</h2>

<p>React is all about re-using code, and it is recommended to split your components into separate files.</p>

<p>To do that, create a new file with a <code className="w3-codespan">.js</code> 
file extension and put the code inside it:</p>

<div className="w3-panel w3-note">
<p>Note that the filename must start with an uppercase character.</p>

</div>


<div className="static-text">
<h3>Example</h3>
<p>This is the new file, we named it "Car.js":</p>

<SyntaxHighlighter language="javascript" style={lightfair} showLineNumbers={true}>
  {codeString6}
</SyntaxHighlighter>

<br/>
</div>

<hr/>

<p>To be able to use the Car component, you have to import the file in your 
application.</p>
<div className="static-text">
<h3>Example</h3>
<p>Now we import the "Car.js" file in the application, and we can use the 
<code className="w3-codespan">Car</code> 
component as if it was created here.</p>

<SyntaxHighlighter className="runexample" language="javascript" style={lightfair} showLineNumbers={true}>
  {codeString7}
</SyntaxHighlighter>


<p>
<a target="_blank" className="w3-btn" href="https://www.w3schools.com/REACT/showreact.asp?filename=demo2_react_comp_in_files">Run 
Example »</a>
</p>

</div>
<hr/>

<hr/>


</div>


  )
}
