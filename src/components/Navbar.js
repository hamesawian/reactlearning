import React from 'react'
import PropTypes from 'prop-types'
import {Outlet, BrowserRouter, Link, Route, Routes } from "react-router-dom";


export default function Navbar() {

  return (

    <div className="sidenav">
                <div id="mySidenav" className="sidenav">
                  <Link to="/" id="react">React</Link>
                  <Link to="/about" id="Components">Components</Link>
                  <Link to="/useeffect" id="projects">useEffect</Link>
                  <Link to="/menu" id="contact">Menu</Link>
                </div>
        <Outlet/>
    </div>
  
  )
}

// Navbar.protoTypes = {
//         title: PropTypes.string.isRequired,
//         aboutText: PropTypes.string
// }

// Navbar.defaultProps = {
//     title: 'Hame',
//     aboutText: 'About us'
// };
