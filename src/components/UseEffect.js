import React from 'react';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { lightfair } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { useState, useEffect } from "react";
import ReactDOM from "react-dom/client";


export default function UseEffect() {


    const codeString1 = `import { useState, useEffect } from "react";
    import ReactDOM from "react-dom/client";
    
    function Timer() {
      const [count, setCount] = useState(0);
    
      useEffect(() => {
        const myTime = 
        setTimeout(() => {
          setCount((count) => count + 1);
        }, 3000)
        if(count === 60){
            clearTimeout(myTime);
        }
      });
    
      return <h1>I've rendered {count} times!</h1>;
    }
    
    const root = ReactDOM.createRoot(document.getElementById('root'));
    root.render(<Timer />);`

   const  codeString2 = `useEffect(() => {
        //Runs on every render
      });`

      const codeString3= `useEffect(() => {
        //Runs only on the first render
      }, []);`

      const codeString4 = `useEffect(() => {
        //Runs on the first render
        //And any time any dependency value changes
      }, [prop, state]);`


      const codeString5 = `import { useState, useEffect } from "react";
      import ReactDOM from "react-dom/client";
      
      function Timer() {
        const [count, setCount] = useState(0);
      
        useEffect(() => {
          setTimeout(() => {
            setCount((count) => count + 1);
          }, 1000);
        }, []); // <- add empty brackets here
      
        return <h1>I've rendered {count} times!</h1>;
      }
      
      const root = ReactDOM.createRoot(document.getElementById('root'));
      root.render(<Timer />);`

      const codeString6 = `import { useState, useEffect } from "react";
      import ReactDOM from "react-dom/client";
      
      function Counter() {
        const [count, setCount] = useState(0);
        const [calculation, setCalculation] = useState(0);
      
        useEffect(() => {
          setCalculation(() => count * 2);
        }, [count]); // <- add the count variable here
      
        return (
          <>
            <p>Count: {count}</p>
            <button onClick={() => setCount((c) => c + 1)}>+</button>
            <p>Calculation: {calculation}</p>
          </>
        );
      }
      
      const root = ReactDOM.createRoot(document.getElementById('root'));
      root.render(<Counter />);`


      const [count, setCount] = useState(0);

      useEffect(() => {
        const myTime = 
        setTimeout(() => {
          setCount((count) => count + 1);
        }, 1000)
        if(count === 60){
            clearTimeout(myTime);
        }
      });
    //for the count down

  


      

  return (
    <div className="Maincontent" id="main">
      

<h1>React <code className="w3-codespan">useEffect</code> Hooks</h1>


<hr/>

<p>The <code className="w3-codespan">useEffect</code> Hook allows you to perform side effects in your components.</p>

<p>Some examples of side effects are: fetching data, directly updating the DOM, and timers.</p>

<p><code className="w3-codespan">useEffect</code> accepts two arguments. The second argument is optional.</p>

<p><code className="w3-codespan">useEffect(&lt;function&gt;, &lt;dependency&gt;)</code></p>

<hr/>

<p>Let's use a timer as an example.</p>

<div className="static-text">
<h3>Example:</h3>
  <p>Use <code className="w3-codespan">setTimeout()</code> to count 1 second after initial render:</p>

  <SyntaxHighlighter className="runexample" language="javascript" style={lightfair} showLineNumbers={true}>
    {codeString1}
  </SyntaxHighlighter>
  <p>
 
</p>

</div>

<p><h3>The result:</h3></p>



    <div className="static-text">
        <h1>I have counted {count} times!</h1>
    </div>
<hr/>

<p>But wait!! It keeps counting even though it should only count once!</p>

<p><code className="w3-codespan">useEffect</code> runs on every render. That means that when the count changes, a render happens, which then triggers another effect.</p>

<p>This is not what we want. There are several ways to control when side effects run.</p>

<p>We should always include the second parameter which accepts an array.
We can optionally pass dependencies to <code className="w3-codespan">useEffect</code> in this array.</p>

<div className="static-text">
<h3>Example</h3>
<p>1. No dependency passed:</p>

<SyntaxHighlighter language="javascript" style={lightfair} showLineNumbers={true}>
    {codeString2}
  </SyntaxHighlighter>

</div>

<div className="static-text">
<h3>Example</h3>
<p>2. An empty array:</p>

<SyntaxHighlighter language="javascript" style={lightfair} showLineNumbers={true}>
    {codeString3}
  </SyntaxHighlighter>

</div>

<div className="static-text">
<h3>Example</h3>
<p>3. Props or state values:</p>

<SyntaxHighlighter language="javascript" style={lightfair} showLineNumbers={true}>
    {codeString4}
  </SyntaxHighlighter>

</div>

<p>So, to fix this issue, let's only run this effect on the initial render.</p>

<div className="static-text">
<h3>Example:</h3>
  <p>Only run the effect on the initial render:</p>

  <SyntaxHighlighter language="javascript" style={lightfair} showLineNumbers={true}>
    {codeString5}
  </SyntaxHighlighter>

<p>
<a target="_blank" className="w3-btn" href="showreact.asp?filename=demo2_react_useeffect_settimeout2">Run 
Example »</a>
</p>
</div>

<div className="static-text">
<h3>Example:</h3>
  <p>Here is an example of a <code className="w3-codespan">useEffect</code> Hook that is dependent on a variable. If the <code className="w3-codespan">count</code> variable updates, the effect will run again:</p>

  <SyntaxHighlighter language="javascript" style={lightfair} showLineNumbers={true}>
    {codeString6}
  </SyntaxHighlighter>

<p>
<a target="_blank" className="w3-btn" href="showreact.asp?filename=demo2_react_useeffect_settimeout3">Run 
Example »</a>
</p>
</div>

<p>If there are multiple dependencies, they should be included in the <code className="w3-codespan">useEffect</code> dependency array.</p>
<hr/>

<hr/>

</div>
  )
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<UseEffect />);
