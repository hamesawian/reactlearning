import React from 'react'

export default function menu() {git 
  return (
	<div>
	 <div className="menu-wrap">
		<nav>
			<ul className="root">
				<li className="current">
					<a href="#menu-1">Menu 1</a>
				</li>
				<a href="#menu-2"> Menu 2
				<span className="down-arrow"></span>
				</a>

				<ul className="level-1">
					<li><a href="#menu-2-1">Menu 2.1</a></li>
					<li><a href="#menu-2-2">Menu 2.2</a></li>
					<li><a href="#menu-2-3">Menu 2.3</a></li>
					
				</ul>
			</ul>
		</nav>
	 </div>
	</div>
  )
}
