import React from 'react'
import SyntaxHighlighter from 'react-syntax-highlighter';
import { lightfair } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import screenshot from './image/helloworld.png';
import firstscreenshot from './image/myfirstreact.png';

export default function react() {


   

    const codeString = `<!DOCTYPE html>
    <html>
      <head>
        <script src="https://unpkg.com/react@18/umd/react.development.js" crossorigin></script>
        <script src="https://unpkg.com/react-dom@18/umd/react-dom.development.js" crossorigin></script>
        <script src="https://unpkg.com/@babel/standalone/babel.min.js"></script>
      </head>
      <body>
    
        <div id="mydiv"></div>
    
        <script type="text/babel">
          function Hello() {
            return <h1>Hello World!</h1>;
          }
    
          const container = document.getElementById('mydiv');
          const root = ReactDOM.createRoot(container);
          root.render(<Hello />)
        </script>
    
      </body>
    </html>`



    const codeString2= `import logo from './logo.svg';
    import './App.css';
    
    function App() {
      return (
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <p>
              Edit <code>src/App.js</code> and save to reload.
            </p>
            <a
              className="App-link"
              href="https://reactjs.org"
              target="_blank"
              rel="noopener noreferrer"
            >
              Learn React
            </a>
          </header>
        </div>
      );
    }
    
    export default App;`


    const codeString3 = `function App() {
      return (
        <div className="App">
          <h1>Hello World!</h1>
        </div>
      );
    }
    
    export default App;`

    const codeString4= `import React from 'react';
    import ReactDOM from 'react-dom/client';
    
    const myFirstElement = <h1>Hello React!</h1>
    
    const root = ReactDOM.createRoot(document.getElementById('root'));
    root.render(myFirstElement);`


  return (

    
      <div className="Maincontent" id="main" >
      

      <h1>React Getting Started</h1>
      
      
      <hr/>
      
      
      
      
      <p className="intro">To get an overview of what React is, you can write React 
      code directly in HTML.</p>
      <p className="intro">But in order to use React in production, you need npm and 
      <a href="https://nodejs.org" target="_blank">Node.js</a> installed.</p>
      
      <hr/>
      
      <h2>React Directly in HTML</h2>
      <p>The quickest way start learning React is to 
      write React directly in your HTML files.</p>
      
      
      
          
          
          
      <p>Start by including three scripts, the first two let us write React code in our 
      JavaScripts, and the third, Babel, allows us to write JSX 
      syntax and ES6 in 
      older browsers.</p>
      <p>You will learn more about JSX in the <a href="react_jsx.asp">React JSX</a> chapter.</p>
      
      <div className="static-text">
            <h3>Example</h3>
            <p>Include three CDN's in your HTML file:</p>

            <SyntaxHighlighter language="javascript" style={lightfair} showLineNumbers={true}>
                {codeString}
            </SyntaxHighlighter>
      </div>
      
      <p>This way of using React can be OK for testing purposes, but for production you will need to set up a 
      <strong>React environment</strong>.</p>
      <hr/>
      
      
      <hr/>
      
      <h2>Setting up a React Environment</h2>
      
      <p>If you have npx and Node.js installed, you can create a React application by 
      using <code className="w3-codespan">create-react-app</code>.</p>
      
      <div className="w3-panel w3-note">
      <p>If you've previously installed <code className="w3-codespan">create-react-app</code> globally,
      it is recommended that you uninstall the package to ensure npx always uses the latest version of <code className="w3-codespan">create-react-app</code>.</p>
      <p>To uninstall, run this command: <code className="w3-codespan">npm uninstall -g create-react-app</code>.</p>
      </div>
      
      <p>Run this command to create a React application named
      <code className="w3-codespan">my-react-app</code>:</p>
      
      <div className="w3-example">
      <div className="w3-code notranslate w3-black">
        npx create-react-app my-react-app
      </div>
      </div>
      
      <p>The <code className="w3-codespan">create-react-app</code> will set up everything you need to run a React application.</p>
      <hr/>
      
      <h2>Run the React Application</h2>
      
      <p>Now you are ready to run your first <em>real</em> React application!</p>
      
      <p>Run this command to move to the <code className="w3-codespan">my-react-app</code> directory:</p>
      
      <div className="w3-example">
      <div className="w3-code notranslate w3-black">
        cd my-react-app
      </div>
      </div>
      
      <p>Run this command to run the React application <code className="w3-codespan">
      my-react-app</code>:</p>
      
      <div className="w3-example">
      <div className="w3-code notranslate w3-black">
        npm start
      </div>
      </div>
      
      <p>A new browser window will pop up with your newly created React App! If not, open your browser and type 
      <code className="w3-codespan">localhost:3000</code> in the address bar.</p>
      
      <p>The result:</p>
      
      <div className="w3-container w3-margin-top">
      <img src={firstscreenshot}/>
      </div>
      <br/>
      <hr/>
      
      <h2>Modify the React Application</h2>
      
      <p>So far so good, but how do I change the content?</p>
      
      <p>Look in the <code className="w3-codespan">my-react-app</code> directory, and you will find a 
      <code className="w3-codespan">src</code> folder. Inside the
      <code className="w3-codespan">src</code> folder there is a file called 
      <code className="w3-codespan">App.js</code>, open it and it will look like this:</p>
      
      <div className="static-text">
          <p>/myReactApp/src/App.js:</p>

          <SyntaxHighlighter language="javascript" style={lightfair} showLineNumbers={true}>
                {codeString2}
            </SyntaxHighlighter>
      
     
      </div>
      
      <p>Try changing the HTML content and save the file.</p>
      
      <div className="w3-panel w3-note">
      <p>Notice that the changes are visible immediately after you save the file, you do not have to reload the browser!</p>
      </div>
      
      <div className="static-text">
      <h3>Example</h3>
      <p>Replace all the content inside the <code className="w3-codespan">&lt;div  classNameName="App"&gt;</code> with a 
      <code className="w3-codespan">&lt;h1&gt;</code> element.</p>
      <p>See the changes in the browser when you click Save.</p>
        
      <SyntaxHighlighter language="javascript" style={lightfair} showLineNumbers={true}>
                {codeString3}
      </SyntaxHighlighter>
      
      <br/>
      </div>
      
      <div className="w3-panel w3-note">
      <p>Notice that we have removed the imports we do not need (logo.svg and App.css).</p>
      </div>
      
      <p>The result:</p>
      
      <div className="w3-container w3-margin-top">
      <img src={screenshot}/>
      </div>
      <hr/>
      
      
  
      
      <hr/>
       
      
      </div>
    
  
  )
}