import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './components/Navbar';
import React from './components/react';
import About from './components/About';
import Menu from './components/menu';
import UseEffect from './components/UseEffect';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import DarkModeToggle from "react-dark-mode-toggle";
import React2, { useState, useEffect } from "react";


function App() {
  const [isDarkMode, setIsDarkMode] = useState(false);

  useEffect(() => {
    const body = document.body;
    if (isDarkMode) {
      body.classList.add('dark-mode');
    } else {
      body.classList.remove('dark-mode');
    }
  }, [isDarkMode]);

  const toggleDarkMode = () => {
    setIsDarkMode(!isDarkMode);
  };


  return (
    <>
    
      <div className="styleDark">
        <DarkModeToggle
          onChange={toggleDarkMode}
          checked={isDarkMode}
          size={60}

          className='DarkToggle'
        />

        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Navbar />}>
              <Route index element={<React />} />
              <Route path="about" element={<About />} />
              <Route path="useeffect" element={<UseEffect />} />
              <Route path="menu" element={<Menu />}/>
            </Route>
          </Routes>
        </BrowserRouter>
      </div>
    </>
  );
}

export default App;
